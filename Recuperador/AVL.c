#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AVL.h"

PATTERN* rotacao_direita(PATTERN *pt){
  PATTERN* ptu;

  ptu = pt->esq;
  pt->esq = ptu->dir;
  ptu->dir = pt;
  pt->FB = 0;
  pt = ptu;
  return pt;
}

PATTERN* rotacao_esquerda(PATTERN *pt){
  PATTERN* ptu;

  ptu = pt->dir;
  pt->dir = ptu->esq;
  ptu->esq = pt;
  pt->FB = 0;
  pt = ptu;
  return pt;
}

PATTERN* rotacao_dupla_direita (PATTERN* pt){
  PATTERN* ptu, *ptv;

  ptu = pt->esq;
  ptv = ptu->dir;
  ptu->dir = ptv->esq;
  ptv->esq = ptu;
  pt->esq = ptv->dir;
  ptv->dir = pt;
  if (ptv->FB == 1)   pt->FB = -1;
  else pt->FB = 0;
  if (ptv->FB == -1)  ptu->FB = 1;
  else ptu->FB = 0;
  pt = ptv;
  return pt;
}

PATTERN* rotacao_dupla_esquerda (PATTERN* pt){
  PATTERN *ptu, *ptv;

  ptu = pt->dir;
  ptv = ptu->esq;
  ptu->esq = ptv->dir;
  ptv->dir = ptu;
  pt->dir = ptv->esq;
  ptv->esq = pt;
  if (ptv->FB == -1) pt->FB = 1;
  else pt->FB = 0;
  if (ptv->FB == 1) ptu->FB = -1;
  else ptu->FB = 0;
  pt = ptv;
  return pt;
}


PATTERN* Caso1 (PATTERN* a , int *ok)
{
  PATTERN *ptu;

  ptu = a->esq;
  if (ptu->FB == 1)
  {
    a = rotacao_direita(a);
  }
  else
  {
    a = rotacao_dupla_direita(a);
  }

  a->FB = 0;
  *ok = 0;
  return a;
}

PATTERN* Caso2 (PATTERN *a , int *ok)
{
  PATTERN *ptu;

  ptu = a->dir;
  if (ptu->FB == -1)
  {
    a=rotacao_esquerda(a);
  }
  else
  {
    a=rotacao_dupla_esquerda(a);
  }
  a->FB = 0;
  *ok = 0;
  return a;
}

PATTERN *InsereAVL (PATTERN *a, char *header, char *end, char *format, int *ok)
{
  /* Insere nodo em uma �rvore AVL, onde A representa a raiz da �rvore,
  x, a chave a ser inserida e h a altura da �rvore */

  if (a == NULL)
  {
    a = (PATTERN*) malloc(sizeof(PATTERN));
    strcpy(a->header, header);
    a->header_size = strlen(header);
    strcpy(a->end, end);
    a->end_size = strlen(end);
    strcpy(a->format, format);

    a->esq = NULL;
    a->dir = NULL;
    a->FB = 0;
    *ok = 1;
  }
  else
  if (strcmp(a->header, header) > 0)
  {
    a->esq = InsereAVL(a->esq, header, end, format, ok);
    if (*ok)
    {
      switch (a->FB) {
        case -1:  a->FB = 0; *ok = 0; break;
        case  0:  a->FB = 1;  break;
        case  1:  a=Caso1(a,ok); break;
      }
    }
  }
  else
  {
    a->dir = InsereAVL(a->dir, header, end, format, ok);
    if (*ok)
    {
      switch (a->FB) {
        case  1:  a->FB = 0; *ok = 0; break;
        case  0:  a->FB = -1; break;
        case -1:  a = Caso2(a,ok); break;
      }
    }
  }
  return a;
}

PATTERN *busca_AVL(PATTERN *a, char *word)
{
  int resultado, ok;

  if (a == NULL)
    return NULL;

  resultado = strncmp(a->header, word, a->header_size);

  if (resultado == 0) {
    return a;
  }
  else if (resultado < 0)
    return busca_AVL(a->dir, word);
  else
    return busca_AVL(a->esq, word);
}

void central_esquerda(PATTERN* a)
{
  if (a!= NULL)
  {
    central_esquerda(a->esq);
    printf("H = %s | S = %d | E = %s | S_END = %d\n", a->header, a->header_size, a->end, a->end_size);
    central_esquerda(a->dir);
  }
}

PATTERN *Destroi(PATTERN* t)
{
  if(t != NULL)
  {
    if(t->esq != NULL) Destroi(t->esq);
    if(t->dir != NULL) Destroi(t->dir);
    free(t);
  }

  return NULL;
}

//
// // Insere um nodo em uma arvore ordenando por peso
// void InsereAVL_peso(PATTERN **a, char *p, double peso, int *ok)
// {
//   if (*a == NULL)
//   {
//     *a = (PATTERN*) malloc(sizeof(PATTERN));
//     strcpy((*a)->palavra, p);
//     (*a)->peso = peso;
//     (*a)->esq = NULL;
//     (*a)->dir = NULL;
//     (*a)->FB = 0;
//     *ok = 1;
//   }
//   else
//   if ((*a)->peso > peso)
//   {
//     InsereAVL_peso(&((*a)->esq),p, peso,ok);
//     if (*ok)
//     {
//       switch ((*a)->FB) {
//         case -1:  (*a)->FB = 0; *ok = 0; break;
//         case  0:  (*a)->FB = 1;  break;
//         case  1:  *a=Caso1(*a,ok); break;
//       }
//     }
//   }
//   else
//   {
//     InsereAVL_peso(&((*a)->dir),p, peso,ok);
//     if (*ok)
//     {
//       switch ((*a)->FB) {
//         case  1:  (*a)->FB = 0; *ok = 0; break;
//         case  0:  (*a)->FB = -1; break;
//         case -1:  *a = Caso2(*a,ok); break;
//       }
//     }
//   }
// }
//
// char* strlwr(char* s)
// {
//   char* tmp = s;
//
//   for (;*tmp;++tmp)
//   *tmp = tolower((unsigned char) *tmp);
//
//   return s;
// }
//
