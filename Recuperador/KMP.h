int *kmp_borders(char *needle, size_t nlen);
char *kmp_search(char *haystack, size_t haylen, char *needle, size_t nlen, int *borders);
char *sstrnstr(char *haystack, char *needle, size_t haylen);
