#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AVL.h"
#include "KMP.h"

#define BLOCK_SIZE 10240 // Tamanho do bloco lido do arquivo
#define PAT_FILE_SIZE 10240 // Tamanho máximo do arquivo com os padrões
#define INTER_BLOCK 3 // Tamanho do recuo feito para não perder padrões entre dois blocos lidos

// print_hex: imprime n chars do conteúdo da memória apontado por ptr em hexadecimal
void print_hex(char *ptr, int n) {
  int i = 0;

  while(i < n) {
    printf("%02hhX ", ptr[i]);

    if ((i+1) % 4 == 0)
      if ((i+1) % 16 == 0)
        printf("\n");
      else
        printf(" ");

   i++;
 }
 printf("--------------------------------------------------\n");
}

// sstrntol: converte a string input em string com chars convertido para base escolhida agrupados em n chars
// ex: sstrntol("FFD8", 2, 16) -> {0xFF, 0xD8, '\0'}
char* sstrntol(char *input, int n, int base) {
  if (input == NULL)
    return NULL;

  int i = 0, j = 0;
  char *str_return;

  char temp[(n + 1)];
  temp[n] = '\0';

  str_return = (char*) malloc(sizeof(char) * (strlen(input)/n) + 1);
  str_return[(strlen(input)/n)] = '\0';

  while (input[0] != '\0') {
    for (i = 0; i < n; i++) {
      temp[i] = input[i];
    }

    input += n;

    str_return[j] = (char) strtol(temp, NULL, base);

    j++;
  }

  return str_return;
}

int main(int argc, char *argv[])
{
// input_file: arquivo analisado | recover_file: arquivo recuperado
FILE *input_file, *recover_file, *patterns_file;

// num_files: número de arquivos recuperados | ok: variável auxiliar usada na AVL
// offset_block: deslocamento feito após cada busca para verificar se encontrou um header válido na árvore de padrões
// num_bytes_read: número de bytes efetivamente lidos do input_file
int num_files = 0, ok = 0, offset_block, num_bytes_read;

// Deslocamentos dos padrões em relação ao início do input_file
long int offset_header, offset_end;

// filename: nome do arquivo recuperado | buffer: buffer que recebe bloco lido do input_file
// ptr_header e ptr_end: aponta para ocorrência localizada do padrão
char filename[32], *ptr_header, *ptr_end, *buffer;

// buffer_patterns: buffer para leitura do patterns_file
// pat_header, pat_end e pat_format: ponteiros auxiliares para inserção na árvore de padrões
char *buffer_patterns, *pat_header, *pat_end, *pat_format;

buffer_patterns = (char*) malloc(sizeof(char) * (PAT_FILE_SIZE + 1));
buffer = (char*) malloc(sizeof(char) * BLOCK_SIZE);

// patterns: Árvore AVL de padrões a serem recuperados | pat_localized: ponteiro para estrutura com o header reconhecido e padrão final a ser recuperado
PATTERN *patterns = NULL, *pat_localized = NULL;

// Validação do número de argumentos da main
if (argc != 3) {
  fprintf(stderr, "Número incorreto de parâmetros.\nUso: %s [ARQUIVO] [PATTERNS]\n", argv[0]);
  return 1;
}

// Abertura do arquivo de entrada
input_file = fopen(argv[1], "rb");

if (input_file == NULL) {
  fprintf(stderr, "Erro ao abrir arquivo de entrada.\n");
  return 1;
}

// Abertura do arquivo de padrões
patterns_file = fopen(argv[2], "r");

if (patterns_file == NULL) {
  fprintf(stderr, "Erro ao abrir arquivo de padrões a serem reconhecidos.\n");
  return 1;
}

// Leitura dos padrões do arquivo patterns_file
fread(buffer_patterns, 1, PAT_FILE_SIZE, patterns_file);

// Separa em strings para inserção na árvore de padrões
pat_header = strtok(buffer_patterns, " \n");
 while (pat_header != NULL) {
   pat_end = strtok(NULL, " \n");
   pat_format = strtok(NULL, " \n");

   printf("%s\n", pat_header);
   printf("%s\n", pat_end);
   printf("%s\n", pat_format);
   patterns = InsereAVL(patterns, sstrntol(pat_header, 2, 16), sstrntol(pat_end, 2, 16), pat_format, &ok);

   pat_header = strtok (NULL, " \n");
 }

free(buffer_patterns); // Libera espaço após ler todo o patterns_file

central_esquerda(patterns);
printf("\n");




//===============================================================================================

num_bytes_read = fread(buffer, 1, BLOCK_SIZE, input_file); // leitura do input_file

// Lê todo o arquivo em busca dos arquivos a ser recuperados
while (num_bytes_read > INTER_BLOCK) {
  // Zera o contador de deslocamento e ponteiro com padrão a ser recuperado
  offset_block = 0;
  pat_localized = NULL;

  // Percorre o bloco lido verificando se a posição atual é um padrão a ser reconhecido
  while (offset_block < num_bytes_read && pat_localized == NULL) {
    pat_localized = busca_AVL(patterns, (buffer + offset_block));
    offset_block++;
  }

  // Header localizado
  if (pat_localized != NULL) {
    // Seta o ponteiro para o header
    ptr_header = buffer + (offset_block - 1);

    // Guarda posição do header no input_file
    offset_header = ftell(input_file) - BLOCK_SIZE + (ptr_header - buffer);

    // Posiciona para o header para buscar padrão final correspondente
    fseek(input_file, (offset_header + pat_localized->header_size), SEEK_SET);

    // Busca do padrão final
    while (fread(buffer, 1, BLOCK_SIZE, input_file) > INTER_BLOCK) {
      // Busca o padrão final no bloco lido
      ptr_end = sstrnstr(buffer, pat_localized->end, BLOCK_SIZE);

      // Padrão final localizado
      if (ptr_end != NULL) {
        // Guarda posição do padrão final no input_file
        offset_end = ftell(input_file) - BLOCK_SIZE + (ptr_end - buffer);
        break; // Encerra busca do padrão final
      }
    }

    // Verifica se foi localizado o padrão final
    if (ptr_end == NULL) {
      // Imprime informações sobre o header
      fprintf(stderr, "Não foi localizado o padrão final correnpondente ao header atual.\n");
      printf("HEADER = %X | %s\n\n", offset_header, pat_localized->format);

      // Posiciona para depois do header
      fseek(input_file, (offset_header + pat_localized->header_size), SEEK_SET);
    }
    else {
      // Dados sobre o arquivo a ser recuperado
      printf("[%d] | %s\n", num_files, pat_localized->format);
      printf("HEADER = %X\n", offset_header);
      printf("END = %X\n", offset_end);
      printf("SIZE = %d bytes\n\n", (offset_end + pat_localized->end_size) - offset_header);

      // Posiciona para o header para iniciar recuperação do arquivo
      fseek(input_file, offset_header, SEEK_SET);

      sprintf(filename, "Recovered/file%04d.%s", num_files, pat_localized->format); // Cria string usada para abertura do arquivo
      recover_file = fopen(filename, "w"); // Abre arquivo a ser recuperado

      // Recupera arquivo
      while (ftell(input_file) < (offset_end + pat_localized->end_size)) {
        fread(buffer, 1, 1, input_file);
        fwrite(buffer, 1, 1, recover_file);
      }

      fclose(recover_file); // Fecha arquivo após ser recuperado

      // Incrementa contador de arquivos e posiciona arquivo para o padrão final
      num_files++;
      fseek(input_file, (offset_end + pat_localized->end_size), SEEK_SET);
    }
  }
  else {
    fseek(input_file, -INTER_BLOCK, SEEK_CUR); // Recuo para poder ler padrões entre dois blocos lidos
  }

  num_bytes_read = fread(buffer, 1, BLOCK_SIZE, input_file); // Nova leitura do input_file
}

printf("Número de arquivos recuperados: %d\n", num_files);
return 0;
}
