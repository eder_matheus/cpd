#define WORD_SIZE 50

typedef struct pat
{
  char header[WORD_SIZE+1];
  int header_size;
  char end[WORD_SIZE+1];
  int end_size;
  char format[WORD_SIZE+1];

  int FB;
  struct pat *esq;
  struct pat *dir;
} PATTERN;

PATTERN *rotacao_direita(PATTERN *pt);
PATTERN *rotacao_esquerda(PATTERN *pt);
PATTERN *rotacao_dupla_direita (PATTERN *pt);
PATTERN *rotacao_dupla_esquerda (PATTERN *pt);
PATTERN *Caso1 (PATTERN *a , int *ok);
PATTERN *Caso2 (PATTERN *a , int *ok);
PATTERN *InsereAVL (PATTERN *a, char *header, char *end, char *format, int *ok);
PATTERN *busca_AVL(PATTERN *a, char *word);
PATTERN *Destroi(PATTERN *t);
void central_esquerda(PATTERN *a);

// void InsereAVL_peso(PATTERN **a, char *p, double peso, int *ok);
// char* strlwr(char *s);
