#include "insertionsort.h"
#include "defs.h"

void insertionsort(std::vector<int> &vector, int size, double &exchanges, double &comparisons, clock_t begin_time) {
	int i, j, actual;

	for (i = 0; i < size; i++) {
		actual = vector[i];
		for (j = i - 1; (j >= 0) && (actual < vector[j]); j--) {
			if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
				comparisons *= -1;
				return;
			}

			vector[j + 1] = vector[j];
			exchanges++;
			comparisons++;
		}
		vector[j + 1] = actual;
		exchanges++;
	}
}
