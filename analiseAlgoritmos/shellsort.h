#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef SHELLSORT_H
#define SHELLSORT_H

void insertionShellsort(std::vector<int> &vector, int size, int initialCell, int inc, double &exchanges, double &comparisons, clock_t begin_time);

void shellsort(std::vector<int> &vector, int size, int numSteps, double &exchanges, double &comparisons, clock_t begin_time);

#endif /* SHELLSORT_H */

