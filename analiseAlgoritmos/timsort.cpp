#include "timsort.h"
#include "defs.h"

void insertionSort(std::vector<int> &vector, int left, int right, double &exchanges, double &comparisons, clock_t begin_time) {
	for (int i = left + 1; i <= right; i++) {
		int temp = vector[i];
		int j = i - 1;

		while (vector[j] > temp && j >= left) {
			vector[j + 1] = vector[j];
			j--;
			exchanges++;
			comparisons++;

			if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
				comparisons *= -1;
				return;
			}
		}
		vector[j + 1] = temp;
		exchanges++;
	}
}

void mergeTim(std::vector<int> &vector, int l, int m, int r, double &exchanges, double &comparisons, clock_t begin_time) {
	int len1 = m - l + 1, len2 = r - m;
	std::vector<int> left;
	std::vector<int> right;
	int i = 0;
	int j = 0;
	int k = l;

	if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
		comparisons *= -1;
		return;
	}

	for (int i = 0; i < len1; i++)
		left.push_back(vector[l + i]);
	for (int i = 0; i < len2; i++)
		right.push_back(vector[m + 1 + i]);

	while (i < len1 && j < len2) {
		if (left[i] <= right[j]) {
			vector[k] = left[i];
			i++;
			comparisons++;
		} else {
			vector[k] = right[j];
			j++;
		}
		exchanges++;
		k++;
		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}
	}

	while (i < len1) {
		vector.insert(vector.begin() + k, left[i]);
		k++;
		i++;
		exchanges++;
		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}
	}

	while (j < len2) {
		vector.insert(vector.begin() + k, right[j]);
		k++;
		j++;
		exchanges++;
		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}
	}
}

void timsort(std::vector<int> &vector, int n, double &exchanges, double &comparisons, clock_t begin_time) {
	for (int i = 0; i < n; i += RUN)
		insertionSort(vector, i, std::min((i + 31), (n - 1)), exchanges, comparisons, begin_time);

	for (int size = RUN; size < n; size = 2 * size) {
		for (int left = 0; left < n; left += 2 * size) {
			int mid = left + size - 1;
			int right = std::min((left + 2 * size - 1), (n - 1));
			mergeTim(vector, left, mid, right, exchanges, comparisons, begin_time);
		}

		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}
	}
}
