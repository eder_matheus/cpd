#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef COMBSORT_H
#define COMBSORT_H

void combsort(std::vector<int> &vector, int size, double &exchanges, double &comparisons, clock_t begin_time);

#endif /* COMBSORT_H */

