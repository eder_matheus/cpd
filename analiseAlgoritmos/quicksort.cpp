#include "quicksort.h"
#include "defs.h"

void swap(int *n1, int *n2, double &exchanges) {
	int change = *n1;
	*n1 = *n2;
	*n2 = change;
	exchanges++;
}

int partition(std::vector<int> &vector, int left, int right, double &exchanges, double &comparisons, clock_t begin_time) {
	int i;
	int j;
	int pivo;

	i = left + 1;
	j = right;
	pivo = left;

	while (j > i) {
		if ((float(clock() - begin_time) / CLOCKS_PER_SEC) > TIMEOUT) {
			comparisons *= -1;
			return 0;
		}

		while (vector[i] < vector[pivo] && i < right) {
			i++;
			comparisons++;
		}
		while (vector[j] >= vector[pivo] && j > left) {
			j--;
			comparisons++;
		}

		if (i < j && vector[i] > vector[j]) {
			swap(&vector[i], &vector[j], exchanges);
			comparisons++;
		}
	}

	if (vector[j] < vector[pivo]) {
		swap(&vector[pivo], &vector[j], exchanges);
		comparisons++;
	}

	return j;
}

void quicksort(std::vector<int> &vector, int i, int f, double &exchanges, double &comparisons, clock_t begin_time) {
	int p;

	if (f > i) {
		p = partition(vector, i, f, exchanges, comparisons, begin_time);

		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}

		quicksort(vector, i, p - 1, exchanges, comparisons, begin_time);
		quicksort(vector, p + 1, f, exchanges, comparisons, begin_time);
	}
}
