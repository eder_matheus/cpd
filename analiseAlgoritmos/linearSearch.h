#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef LINEARSEARCH_H
#define LINEARSEARCH_H

void linearSearch(std::vector<int> &vector, int num, double &exchanges, double &comparisons);

#endif /* LINEARSEARCH_H */

