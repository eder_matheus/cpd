#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef MERGESORT_H
#define MERGESORT_H

#ifdef __cplusplus
extern "C" {
#endif

    void merge(std::vector<int> &vector, int l, int m, int r, double &exchanges, double &comparisons, clock_t begin_time);
    
    void mergesort(std::vector<int> &vector, int l, int r, double &exchanges, double &comparisons, clock_t begin_time);

#ifdef __cplusplus
}
#endif

#endif /* MERGESORT_H */

