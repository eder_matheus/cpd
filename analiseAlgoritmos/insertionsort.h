#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

void insertionsort(std::vector<int> &vector, int size, double &exchanges, double &comparisons, clock_t begin_time);

#endif /* INSERTIONSORT_H */

