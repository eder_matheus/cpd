#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef HEAPSORT_H
#define HEAPSORT_H

typedef struct {
    int size;
    int length;
    std::vector<int> vector;
} HEAP;

void swapHeap(HEAP &heap, int i, int major, double &exchanges);

int length(std::vector<int> vector);

void heapify(HEAP &heap, int i, double &exchanges, double &comparisons);

void buildHeap(HEAP &heap, std::vector<int> const &vector, double &exchanges, double &comparisons);

void heapsort(std::vector<int> &vector, double &exchanges, double &comparisons, clock_t begin_time);

#endif /* HEAPSORT_H */

