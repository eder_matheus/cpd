#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

const int RUN = 32;

#ifndef TIMSORT_H
#define TIMSORT_H

void insertionSort(std::vector<int> &vector, int left, int right, double &exchanges, double &comparisons, clock_t begin_time);

void mergeTim(std::vector<int> &vector, int l, int m, int r, double &exchanges, double &comparisons, clock_t begin_time) ;

void timsort(std::vector<int> &vector, int n, double &exchanges, double &comparisons, clock_t begin_time);

#endif /* TIMSORT_H */

