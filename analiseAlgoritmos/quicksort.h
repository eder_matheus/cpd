#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef QUICKSORT_H
#define QUICKSORT_H

#ifdef __cplusplus
extern "C" {
#endif

    void swap(int *n1, int *n2, double &exchanges);

    int partition(std::vector<int> &vector, int left, int right, double &exchanges, double &comparisons, clock_t begin_time);

    void quicksort(std::vector<int> &vector, int i, int f, double &exchanges, double &comparisons, clock_t begin_time);


#ifdef __cplusplus
}
#endif

#endif /* QUICKSORT_H */

