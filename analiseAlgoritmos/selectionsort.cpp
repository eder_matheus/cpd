#include "selectionsort.h"
#include "defs.h"

void selectionsort(std::vector<int> &vector, int n, double &exchanges, double &comparisons, clock_t begin_time) {
	int i, j, min, aux;

	for (i = 0; i < (n - 1); i++) {
		min = i;
		for (j = (i + 1); j < n; j++) {
			if (vector[j] < vector[min]) {
				min = j;
				comparisons++;
			}
		}

		if (vector[i] != vector[min]) {
			aux = vector[i];
			vector[i] = vector[min];
			vector[min] = aux;
			comparisons++;
			exchanges++;
		}

		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}
	}
}
