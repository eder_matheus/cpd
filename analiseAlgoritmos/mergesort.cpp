#include "mergesort.h"
#include "defs.h"

void merge(std::vector<int> &vector, int l, int m, int r, double &exchanges, double &comparisons, clock_t begin_time) {
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;

	/* create temp arrays */
	std::vector<int> L;
	std::vector<int> R;

	/* Copy data to temp arrays L[] and R[] */
	for (i = 0; i < n1; i++)
		L.push_back(vector[l + i]);
	for (j = 0; j < n2; j++)
		R.push_back(vector[m + 1 + j]);

	if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
		comparisons *= -1;
		return;
	}

	/* Merge the temp arrays back into arr[l..r]*/
	i = 0;
	j = 0;
	k = l;
	while (i < n1 && j < n2) {
		if (L[i] <= R[j]) {
			comparisons++;
			vector[k] = L[i];
			exchanges++;
			i++;
		} else {
			vector[k] = R[j];
			exchanges++;
			j++;
		}
		k++;
	}

	/* Copy the remaining elements of L[], if there
	   are any */
	while (i < n1) {
		comparisons++;
		vector[k] = L[i];
		exchanges++;
		i++;
		k++;
	}

	/* Copy the remaining elements of R[], if there
	   are any */
	while (j < n2) {
		comparisons++;
		vector[k] = R[j];
		exchanges++;
		j++;
		k++;
	}
}

void mergesort(std::vector<int> &vector, int l, int r, double &exchanges, double &comparisons, clock_t begin_time) {
	if (l < r) {
		int m = l + (r - l) / 2;

		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}

		mergesort(vector, l, m, exchanges, comparisons, begin_time);
		mergesort(vector, m + 1, r, exchanges, comparisons, begin_time);

		merge(vector, l, m, r, exchanges, comparisons, begin_time);
	}
}
