#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef BINARYSEARCH_H
#define BINARYSEARCH_H

void binarySearch(std::vector<int> &vector, int num, double &exchanges, double &comparisons);

#endif /* BINARYSEARCH_H */

