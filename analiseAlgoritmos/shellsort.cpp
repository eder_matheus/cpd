#include "shellsort.h"
#include "defs.h"

void insertionShellsort(std::vector<int> &vector, int size, int initialCell, int inc, double &exchanges, double &comparisons, clock_t begin_time) {
	int i, j, key;

	for (j = (initialCell + inc); j < size; j += inc) {
		if ((float(clock() - begin_time) / CLOCKS_PER_SEC) > TIMEOUT) {
			comparisons *= -1;
			return;
		}

		key = vector[j];
		i = j - inc;

		while ((i >= 0) && (vector[i] > key)) {
			vector[i + inc] = vector[i];
			exchanges++;
			i = i - inc;
			comparisons++;
		}

		vector[i + inc] = key;
		exchanges++;
	}
}

void shellsort(std::vector<int> &vector, int size, int numSteps, double &exchanges, double &comparisons, clock_t begin_time) {
	int p, h, j;

	for (p = numSteps; p > 1; p--) {
		if ((float(clock() - begin_time) / CLOCKS_PER_SEC) > TIMEOUT) {
			comparisons *= -1;
			return;
		}

		h = (int) (std::pow(2.0, (float) (p - 1)));
		for (j = 0; j < h - 1; j++)
			insertionShellsort(vector, size, j, (h - 1), exchanges, comparisons, begin_time);
	}
}
