#include <cstdlib>
#include <iostream>
#include <cstdlib>
#include <random>
#include <ctime>
#include <cmath>
#include <vector>

#ifndef SELECTIONSORT_H
#define SELECTIONSORT_H

void selectionsort(std::vector<int> &vector, int n, double &exchanges, double &comparisons, clock_t begin_time);

#endif /* SELECTIONSORT_H */

