#include "combsort.h"
#include "defs.h"

void combsort(std::vector<int> &vector, int size, double &exchanges, double &comparisons, clock_t begin_time) {
	int i, j, gap = size;
	int exchanged = 1;
	int aux;

	while (gap > 1 && exchanged == 1) {
		gap = (gap * 10) / 13;
		if (gap == 9 || gap == 10) {
			gap = 11;
		} else if (gap < 1) {
			gap = 1;
		}
		comparisons++;
		for (i = 0, j = gap; j < size; i++, j++) {
			if (vector[i] > vector[j]) {
				comparisons++;
				aux = vector[i];
				vector[i] = vector[j];
				vector[j] = aux;
				exchanged = 1;
				exchanges++;
			}
		}

		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}
	}
}
