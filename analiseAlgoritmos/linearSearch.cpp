#include "linearSearch.h"

void linearSearch(std::vector<int> &vector, int num, double &exchanges, double &comparisons) {
	int i = 0;
	int found = 0;

	while (found == 0 && i < vector.size()) {
		comparisons++;
		if (vector[i] == num) {
			found = 1;
			std::cout << num << " found\n";
			return;
		}
		i++;
	}

	std::cout << "Number not found\n";
}
