#include "combsort.h"
#include "binarySearch.h"

void binarySearch(std::vector<int> &vector, int num, double &exchanges, double &comparisons) {
	int inf = 0;
	int sup = vector.size() - 1;
	int middle;
	while (inf <= sup) {
		comparisons++;
		middle = (inf + sup) / 2;
		comparisons++;
		if (num == vector[middle]) {
			std::cout << num << " found\n";
			return;
		}
		comparisons++;
		if (num < vector[middle])
			sup = middle - 1;
		else
			inf = middle + 1;
	}

	std::cout << "Number not found\n";
	return; // não encontrado
}
