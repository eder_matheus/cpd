#include "heapsort.h"
#include "defs.h"

void swapHeap(HEAP &heap, int i, int major, double &exchanges) {
	int aux = heap.vector[i];
	heap.vector[i] = heap.vector[major];
	heap.vector[major] = aux;
	exchanges++;
}

int length(std::vector<int> vector) {
	int height;
	height = std::floor(std::log2(vector.size()) + 1);
	return int(std::pow(2.0, (float) height) - 1);
}

void heapify(HEAP &heap, int i, double &exchanges, double &comparisons) {
	int left, right, major;

	left = (i * 2) + 1;
	right = (i * 2) + 2;
	major = i;

	if (left < heap.size && (heap.vector[left] > heap.vector[major])) {
		major = left;
		comparisons++;
	}
	if (right < heap.size && (heap.vector[right] > heap.vector[major])) {
		major = right;
		comparisons++;
	}

	if (major != i) {
		swapHeap(heap, i, major, exchanges);
		heapify(heap, major, exchanges, comparisons);
	}
}

void buildHeap(HEAP &heap, std::vector<int> const &vector, double &exchanges, double &comparisons) {
	heap.vector = vector;
	heap.size = vector.size();
	heap.length = length(vector);

	for (int i = ((heap.length / 2) - 1); i >= 0; i--)
		heapify(heap, i, exchanges, comparisons);
}

void heapsort(std::vector<int> &vector, double &exchanges, double &comparisons, clock_t begin_time) {
	HEAP heap;

	buildHeap(heap, vector, exchanges, comparisons);

	for (int i = vector.size() - 1; i >= 1; i--) {
		swapHeap(heap, 0, i, exchanges);
		heap.size--;
		heapify(heap, 0, exchanges, comparisons);

		if (float(clock() - begin_time) / CLOCKS_PER_SEC > TIMEOUT) {
			comparisons *= -1;
			return;
		}
	}

	vector.clear();
	for (int i = 0; i < heap.vector.size(); i++)
		vector.push_back(heap.vector[i]);
}
