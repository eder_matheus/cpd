#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "insertionsort.h"
#include "shellsort.h"

#include "combsort.h"
#include "quicksort.h"

#include "selectionsort.h"
#include "heapsort.h"

#include "mergesort.h"
#include "timsort.h"

#include "binarySearch.h"
#include "linearSearch.h"
#include "defs.h"

#include <vector>
#include <ctime>
#include <cmath>
#include <fstream>

int main() {

	double exchanges = 0;
	double comparisons = 0;
	int numElements = 1000;
	int numSort;

	std::vector<int> randomArray;
	std::vector<int> orderArray;
	std::vector<int> invArray;
	std::vector<int> randomVector;
	std::vector<int> orderVector;
	std::vector<int> invVector;

	int x; // Used as an aux to read the binary file
	float runtime;
	int num = 1438215;

	std::ifstream infile;
	std::ofstream outfile;
	clock_t begin_time = clock();

	infile.open("randomnumbers.bin", std::ios::binary | std::ios::in);

	std::cout << "Creating array \n";
	while (!infile.eof()) {
		infile.read(reinterpret_cast<char*> (&x), sizeof (int));
		randomArray.push_back(x);
	}

	infile.close();

	std::cout << "Array created \n";

	orderArray = randomArray;

	std::cout << "Ordering array \n";
	begin_time = clock();
	combsort(orderArray, orderArray.size(), exchanges, comparisons, begin_time);
	std::cout << "Array ordered \n";

	outfile.open("R00274726-00277950-00274744.txt");

	std::cout << "\nLinear search: \n";

	comparisons = 0;
	exchanges = 0;

	begin_time = clock();
	linearSearch(orderArray, num, exchanges, comparisons);
	runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
	outfile << "BLin, O, 10000000, " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";

	std::cout << "Binary search: \n";

	comparisons = 0;
	exchanges = 0;

	begin_time = clock();
	binarySearch(orderArray, num, exchanges, comparisons);
	runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
	outfile << "Bbin, O, 10000000, " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";

	std::cout << "\nInverting array \n";
	for (int i = 0; i < orderArray.size(); i++) {
		invArray.push_back(orderArray[orderArray.size() - 1 - i]);
	}
	std::cout << "Array inverted \n";

	// For each iteration, an algorithm is executed 5 times, for the five
	// different size of array
	// The maximum time of execution of an algorithm is 10 min!!!
	for (numSort = 1; numSort <= 8; numSort++) {
		numElements = 1000;
		exchanges = 0;
		comparisons = 0;

		do {
			randomVector.assign(randomArray.begin(), randomArray.begin() + numElements);
			orderVector.assign(orderArray.begin(), orderArray.begin() + numElements);
			invVector.assign(invArray.begin(), invArray.begin() + numElements);

			switch (numSort) {
				case 1:
					std::cout << "ShellSort with " << numElements << "\n";
					begin_time = clock();
					shellsort(randomVector, randomVector.size(), 4, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "SheS, R, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "SheS, R, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					shellsort(orderVector, orderVector.size(), 4, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "SheS, O, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "SheS, O, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					shellsort(invVector, invVector.size(), 4, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "SheS, I, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "SheS, I, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					break;
				case 2:
					std::cout << "QuickSort with " << numElements << "\n";
					begin_time = clock();
					quicksort(randomVector, 0, randomVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "QukS, R, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "QukS, R, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					quicksort(orderVector, 0, orderVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "QukS, O, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "QukS, O, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					quicksort(invVector, 0, invVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "QukS, I, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "QukS, I, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					break;
				case 3:
					std::cout << "HeapSort with " << numElements << "\n";
					begin_time = clock();
					heapsort(randomVector, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "HepS, R, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "HepS, R, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					heapsort(orderVector, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "HepS, O, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "HepS, O, " << numElements << ", *, *, *" << "\n";
					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					heapsort(invVector, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "HepS, I, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "HepS, I, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					break;
				case 4:
					std::cout << "TimSort with " << numElements << "\n";
					begin_time = clock();
					timsort(randomVector, randomVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "TimS, R, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "TimS, R, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					timsort(orderVector, orderVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "TimS, O, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "TimS, O, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					timsort(invVector, invVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "TimS, I, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "TimS, I, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					break;
				case 5:
					std::cout << "InsertionSort with " << numElements << "\n";
					begin_time = clock();
					insertionsort(randomVector, randomVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "ISBL, R, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "ISBL, R, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					insertionsort(orderVector, orderVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "ISBL, O, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "ISBL, O, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					insertionsort(invVector, invVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "ISBL, I, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "ISBL, I, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					break;
				case 6:
					std::cout << "CombSort with " << numElements << "\n";
					begin_time = clock();
					combsort(randomVector, randomVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "CbSt, R, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "CbSt, R, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					combsort(orderVector, orderVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "CbSt, O, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "CbSt, O, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					combsort(invVector, invVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "CbSt, I, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "CbSt, I, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					break;
				case 7:
					std::cout << "SelectionSort with " << numElements << "\n";
					begin_time = clock();
					selectionsort(randomVector, randomVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "SelS, R, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "SelS, R, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					selectionsort(orderVector, orderVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "SelS, O, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "SelS, O, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					selectionsort(invVector, invVector.size(), exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "SelS, I, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "SelS, I, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					break;
				case 8:
					std::cout << "MergeSort with " << numElements << "\n";
					begin_time = clock();
					mergesort(randomVector, 0, randomVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "MerS, R, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "MerS, R, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					mergesort(orderVector, 0, orderVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "MerS, O, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime * 1000 << "\n";
					else
						outfile << "MerS, O, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					begin_time = clock();
					mergesort(invVector, 0, invVector.size() - 1, exchanges, comparisons, begin_time);
					runtime = float (clock() - begin_time) / CLOCKS_PER_SEC;
					if (comparisons > 0)
						outfile << "MerS, I, " << numElements << ", " << exchanges << ", " << comparisons << ", " << runtime << "\n";
					else
						outfile << "MerS, I, " << numElements << ", *, *, *" << "\n";

					exchanges = 0;
					comparisons = 0;

					break;
			}

			numElements *= 10;
			randomVector.clear();
			orderVector.clear();
			invVector.clear();
		} while (numElements <= 10000000);
	}

	outfile.close();

	return 0;
}
